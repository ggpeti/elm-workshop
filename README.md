# Functional web app development in Elm
## Outline of the workshop
### Motivation
JS is the language of the web. It runs on almost every device, in browsers, on servers via nodejs, on desktop computers via Electron. It is a simple and great language with amazing tooling, but it lacks a few features such as static type checking that could catch errors up front, or the ability to guarantee that a piece of code does not affect the environment. If you are running JS code on nodejs, anywhere in the code you are able to make network requests, write into the database, or even delete files without the slightest warning.

Enter Elm: a web language that is statically typed and side effect free. It is a language built with immutability in mind from the ground up. That means it's impossible to reassign a variable or to change the value of an object, which makes programmers trust their code on another level. More than that, it compiles to Javascript and you can interact with an embedded Elm program as simply as `program.ports.myInputPort.send(value)` and `program.ports.myOutputPort.subscribe(handler)`.

So if Elm is side effect free, how are we even able to do something? We'll get back to that. Let's try writing our first Elm app now.

### First Elm App
Elm is a wonderful way to write type safe HTML. We are going to write a web app that responds to our clicks and text input.

1. Install elm. `npm i -g elm`
2. Install elm-format and set up your coding environment as described [here](https://github.com/avh4/elm-format#jetbrains-installation)
3. Create a new directory and do `git init`.
4. Run `elm-package install`. This sets up an `elm-package.json` which serves the same purpose for Elm as `package.json` does for nodejs.
5. Create file `Main.elm` and code along.

The code from the workshop can be found in the helloworld directory. Run the rest of the commands from there.

### Package the app into Electron
We are creating a small wrapper for our Elm program and bundle it into a desktop app with electron-packager, an npm package.
1. `npm init`
2. `npm i electron electron-packager url`
3. Compile the Elm app into html via `elm-make Main.elm --output app.js`
4. Run `node_modules/.bin/electron-packager .` - this generates an .app if you are on Mac, a .exe if you are on Windows.
5. Launch the app.
