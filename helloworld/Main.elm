module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


type Color
    = IndianRed
    | AliceBlue
    | CornflowerBlue


type alias Model =
    { color : Color
    , inputText : String
    }


type Msg
    = ChangeColor
    | TextEntered String


init : Model
init =
    { color = IndianRed
    , inputText = ""
    }


main : Program Never Model Msg
main =
    beginnerProgram { model = init, view = view, update = update }


update : Msg -> Model -> Model
update msg model =
    case msg of
        ChangeColor ->
            case model.color of
                IndianRed ->
                    { model | color = AliceBlue }

                AliceBlue ->
                    { model | color = CornflowerBlue }

                CornflowerBlue ->
                    { model | color = IndianRed }

        TextEntered str ->
            { model | inputText = str }


view : Model -> Html Msg
view model =
    div
        [ style
            [ ( "font-size", "10em" )
            , ( "color", toString model.color )
            ]
        , onClick ChangeColor
        ]
        [ input [ onInput TextEntered ] []
        , viewHello model
        ]


viewHello : Model -> Html Msg
viewHello model =
    text
        (if model.inputText == "" then
            "Hello World"
         else
            model.inputText
        )
